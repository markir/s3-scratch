#!/usr/bin/python3

import argparse
from s3.min import client

parser = argparse.ArgumentParser()
parser.add_argument("-u", "--access", help="access", default="bfcb11397be346ce99870a2800d650c9")
parser.add_argument("-k", "--secret", help="secret", default="47a669005a454cd2bc8fccca9b5f93c9")
parser.add_argument("-x", "--host", help="host", default="proxy2")
parser.add_argument("-p", "--port", help="port", default="8080")
parser.add_argument("-l", "--url", help="url", default="/con1")
parser.add_argument("-a", "--act", help="act", default="HEAD")
parser.add_argument("-r", "--region", help="region", default='us-east-1')
parser.add_argument("-d", "--data", help="data", default=None)
parser.add_argument("-s", "--secure", help="secure", default=False)
args = parser.parse_args()

conn = client(args.host,
              args.port,
              args.url,
              args.act,
              args.access,
              args.secret,
              args.region,
              args.data,
              args.secure)

conn.setauth()

response = conn.head()

print("response: %s" %(response))

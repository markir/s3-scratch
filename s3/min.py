#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import hmac
import hashlib
from datetime import datetime, timezone
import requests
import xmltodict


# Ancillary functions
#
def simplehmac(key, msg):
    h = hmac.new(
            key,
            msg.encode(),
            hashlib.sha256)

    return h.digest()


def simplesha256(var):
    h = hashlib.sha256(var.encode())

    return h.hexdigest()


def wantlist(var):
    # make sure we always return a list
    if type(var) is list:
        return var
    else:
        return [var]


# Classes
#
class client(object):
    """
    A minimal from scratch S3 client for V4 signature supporting servers.
    As designed, it needs to live in a dir called s3 and be named min.py.

    Example usage:

    from s3.min import client

    conn = client('localhost',
                  '8080',
                  '/con1',
                  'GET',
                  'my access',
                  'my secret',
                  'us-east-1',
                  '')

    conn.setauth()

    response = conn.get()

    print("response: %s" %(response))
    """
    def __init__(self,
                 host,
                 port,
                 url,
                 act,
                 access,
                 secret,
                 region='us-east-1',
                 data=None,
                 secure=False):
        """
        Set basic elements
        """
        self.host = host
        self.port = port
        self.url = url
        self.act = act
        self.access = access
        self.secret = secret
        self.region = region
        self.service = 's3'
        self.request_type = 'aws4_request'
        self.date = None
        self.partdate = None
        self.data = data
        self.stringtosign = None
        self.auth = None
        self.secure = secure

        if self.secure:
            self.prot = 'https'
        else:
            self.prot = 'http'

        if self.act not in ['HEAD', 'GET', 'PUT', 'POST', 'DELETE']:
            raise  Exception("invalid HTTP action (must be upper case)" + self.act)

        if url[0:1] != '/':
            raise  Exception("url must begin with /")

        if data == None:
           self.content = simplesha256("")
        else:
           self.content = simplesha256(data)


    def setauth(self):
        """
        Set the authorization header.

        Call this guy after init, it does all the signature calc work!
        """
        self._setdate()
        self._setstringtosign()
        self._setsignkey()
        self._setsig()

        self.auth = "AWS4-HMAC-SHA256 Credential=%s/%s/%s/%s/%s,SignedHeaders=host;x-amz-content-sha256;x-amz-date,Signature=%s" % (self.access, self.partdate, self.region, self.service, self.request_type, self.sig.hex())


    def get(self):
        """
        Get the url.

        Call this after setauth
        """
        if self.act != 'GET':
            raise  Exception("cannot call get if HTTP action is %s)" + self.act)
        if self.auth == None:
            raise  Exception("cannot get with no authorization header (call setauth 1st)")

        acturl = "%s://%s:%s%s" % (self.prot, self.host, self.port, self.url)
        actheaders = {
              'Host': self.host,
              'Authorization': self.auth,
              'x-amz-date': self.date,
              'x-amz-content-sha256': self.content}

        response = requests.get(
             acturl,
             headers = actheaders)
       
        return response


    def head(self):
        """
        Head the url.

        Call this after setauth
        """
        if self.act != 'HEAD':
            raise  Exception("cannot call head if HTTP action is %s)" + self.act)
        if self.auth == None:
            raise  Exception("cannot head with no authorization header (call setauth 1st)")

        acturl = "%s://%s:%s%s" % (self.prot, self.host, self.port, self.url)
        actheaders = {
              'Host': self.host,
              'Authorization': self.auth,
              'x-amz-date': self.date,
              'x-amz-content-sha256': self.content}

        response = requests.head(
             acturl,
             headers = actheaders)
       
        return response


    def put(self):
        """
        Put the url.

        Call this after setauth
        """
        if self.act != 'PUT':
            raise  Exception("cannot call put if HTTP action is %s)" + self.act)
        if self.auth == None:
            raise  Exception("cannot put with no authorization header (call setauth 1st)")

        acturl = "%s://%s:%s%s" % (self.prot, self.host, self.port, self.url)
        actheaders = {
              'Host': self.host,
              'Authorization': self.auth,
              'x-amz-date': self.date,
              'x-amz-content-sha256': self.content}

        response = requests.put(
             acturl,
             data = self.data,
             headers = actheaders)
       
        return response


    def delete(self):
        """
        Delete the url.

        Call this after setauth
        """
        if self.act != 'DELETE':
            raise  Exception("cannot call delete if HTTP action is %s)" + self.act)
        if self.auth == None:
            raise  Exception("cannot delete with no authorization header (call setauth 1st)")

        acturl = "%s://%s:%s%s" % (self.prot, self.host, self.port, self.url)
        actheaders = {
              'Host': self.host,
              'Authorization': self.auth,
              'x-amz-date': self.date,
              'x-amz-content-sha256': self.content}

        response = requests.delete(
             acturl,
             headers = actheaders)
       
        return response


    def getbucketcontents(self, response):
        """
        Parse the Get result for a bucket and extract contents. Format as
        JSON instead of XML.

        Example usage:

        conn.seturl('/con1',
                    'GET')
        conn.setauth()

        response = conn.get()

        if response.status_code == 200:
            bucketcontents = conn.getbucketcontents(response)

            for obj in bucketcontents:
                print("object: %s\t%s" % (obj['Key'], obj['ETag']))
        """
        dictresponse = xmltodict.parse(response.text)

        # If the bucket has no objects, then there is no 'Contents' 
        # key at all. S3 is weird.
        if 'Contents' not in dictresponse['ListBucketResult'].keys():
            bucketcontents = []
        else:
            bucketcontents = dictresponse['ListBucketResult']['Contents']

        return wantlist(bucketcontents)


    def getallbuckets(self, response):
        """
        Parse the Get for an account (i.e url '/') and extract contents.
        Format as JSON instead of XML.

        Example usage:

        conn.seturl('/',
                    'GET')
        conn.setauth()

        response = conn.get()

        if response.status_code == 200:
            buckets = conn.getallbuckets(response)
            for bucket in buckets:
                print("bucket: %s" %(bucket['Name']))
        """
        dictresponse = xmltodict.parse(response.text)

        # If there are no buckets then the 'Buckets' value is None
        # S3 is weird (contrast with bucket listing above!
        if dictresponse['ListAllMyBucketsResult']['Buckets'] == None:
            buckets = []
        else:
            buckets = dictresponse['ListAllMyBucketsResult']['Buckets']['Bucket']
 
        return wantlist(buckets)


    def seturl(self,
               url,
               act,
               data=None):
        """
        Set a new url (plus perhaps action and data).

        Use this to change url w/o re-init. A call to
        setauth is required however to recompute sig etc.

        Example usage:

        conn.seturl('/con/newobj',
                    'GET')
        conn.setauth()

        response = conn.get()
        """
        self.url = url
        self.act = act
        self.data = data

        self.auth = None

        if data == None:
           self.content = simplesha256("")
        else:
           self.content = simplesha256(data)


    def _setdate(self):
        self.date = (datetime.now(timezone.utc)).strftime('%Y%m%dT%H%M%SZ')
        self.partdate = self.date[0:8]


    def _setstringtosign(self):
        canonical_req = """%s
%s

host:%s
x-amz-content-sha256:%s
x-amz-date:%s

host;x-amz-content-sha256;x-amz-date
%s""" % (self.act, self.url, self.host, self.content, self.date, self.content)

        scope = "%s/%s/%s/%s" % (self.partdate, self.region, self.service, self.request_type)
        req_hash = simplesha256(canonical_req)

        self.stringtosign = """AWS4-HMAC-SHA256
%s
%s
%s""" % (self.date, scope, req_hash)


    def _setsignkey(self):
        datekey =       simplehmac(
                           ('AWS4' + self.secret).encode(),
                           self.partdate)
        dateregkey =    simplehmac(
                           datekey,
                           self.region)
        dateregsrvkey = simplehmac(
                           dateregkey,
                           self.service)
        self.signkey  = simplehmac(
                           dateregsrvkey,
                           self.request_type)


    def _setsig(self):
        self.sig =      simplehmac(
                           self.signkey,
                           self.stringtosign)
